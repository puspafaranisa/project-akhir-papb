package com.tugasakhirpapb.ruangbelajar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tugasakhirpapb.ruangbelajar.model.MataKuliah;

public class EditMatkulActivity extends AppCompatActivity {

    private EditText namaInput, dosenInput;
    private Button editButton;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mDBMatkulRef;

    private MataKuliah mataKuliah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_matkul);

        Intent intent = getIntent();
        String matkulID = intent.getStringExtra("ID");

        namaInput = findViewById(R.id.JudulInput);
        dosenInput = findViewById(R.id.DeadlineInput);
        editButton = findViewById(R.id.EditButton);

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateKelas();
            }
        });

        //INSTANSIASI FIREBASE DATABASE
        mDatabase = FirebaseDatabase.getInstance("https://ruang-belajar-6570f-default-rtdb.asia-southeast1.firebasedatabase.app/");

        //REFERENSI DATABASE KE TABEL MATAKULIAH
        mDBMatkulRef = mDatabase.getReference("MataKuliah").child(matkulID);

        mDBMatkulRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                mataKuliah = snapshot.getValue(MataKuliah.class);
                mataKuliah.setId(snapshot.getKey());
                UpdateUI();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(EditMatkulActivity.this, "Kesalahan Memuat Data", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void UpdateUI() {
        namaInput.setText(mataKuliah.getNamaMataKuliah());
        dosenInput.setText(mataKuliah.getDosenPengampu());
    }

    private void UpdateKelas() {
        mataKuliah.setNamaMataKuliah(namaInput.getText().toString());
        mataKuliah.setDosenPengampu(dosenInput.getText().toString());

        mDBMatkulRef.setValue(mataKuliah);
        Toast.makeText(EditMatkulActivity.this, "Data Berhasil Di Ubah", Toast.LENGTH_LONG).show();
        finish();
    }
}