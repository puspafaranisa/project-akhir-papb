package com.tugasakhirpapb.ruangbelajar.controller;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tugasakhirpapb.ruangbelajar.R;
import com.tugasakhirpapb.ruangbelajar.DetailTugasActivity;
import com.tugasakhirpapb.ruangbelajar.EditTugasActivity;
import com.tugasakhirpapb.ruangbelajar.model.Tugas;

import java.util.ArrayList;
import java.util.List;

public class TugasAdapter extends RecyclerView.Adapter<TugasAdapter.TugasViewHolder> {

    private Context context;
    private List<Tugas> tugasList;

    public  TugasAdapter(Context context, ArrayList<Tugas> tugasList) {
        this.context = context;
        this.tugasList = tugasList;
    }

    @NonNull
    @Override
    public TugasAdapter.TugasViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tugas, parent, false);
        return new TugasAdapter.TugasViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TugasAdapter.TugasViewHolder holder, int position) {
        final Tugas tugas = tugasList.get(position);

        holder.judulView.setText(tugas.getJudul());
        holder.descView.setText(tugas.getDeskripsi());
        holder.deadlineView.setText(tugas.getDeadline());

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent detailActivity = new Intent(context, DetailTugasActivity.class);
                detailActivity.putExtra("ID", tugas.getId());
                context.startActivity(detailActivity);
            }
        });

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent editActivity = new Intent(context, EditTugasActivity.class);
                editActivity.putExtra("ID", tugas.getId());
                context.startActivity(editActivity);
            }
        });

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseDatabase db = FirebaseDatabase.getInstance("https://ruang-belajar-6570f-default-rtdb.asia-southeast1.firebasedatabase.app/");
                DatabaseReference ref = db.getReference().child("Tugas").child(tugas.getId());
                ref.getRef().removeValue();
            }
        });
    }

    @Override
    public int getItemCount() {
        return tugasList.size();
    }

    public class TugasViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout container;
        public TextView judulView, descView, deadlineView;
        public Button editButton, deleteButton;

        public TugasViewHolder(@NonNull View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.ItemTugasContainer);
            judulView = itemView.findViewById(R.id.JudulTugasTextView);
            descView = itemView.findViewById(R.id.DescTugasTextView);
            deadlineView = itemView.findViewById(R.id.DeadlineTugasTextView);
            editButton = itemView.findViewById(R.id.EditButton);
            deleteButton = itemView.findViewById(R.id.DeleteButton);
        }
    }
}