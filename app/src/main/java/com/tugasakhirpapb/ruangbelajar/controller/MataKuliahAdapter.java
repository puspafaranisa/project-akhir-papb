package com.tugasakhirpapb.ruangbelajar.controller;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tugasakhirpapb.ruangbelajar.R;
import com.tugasakhirpapb.ruangbelajar.DetailMatkulActivity;
import com.tugasakhirpapb.ruangbelajar.EditMatkulActivity;
import com.tugasakhirpapb.ruangbelajar.model.MataKuliah;

import java.util.ArrayList;
import java.util.List;

public class MataKuliahAdapter extends RecyclerView.Adapter<MataKuliahAdapter.MataKuliahViewHolder> {

    private Context context;
    private List<MataKuliah> mataKuliahList;

    public  MataKuliahAdapter(Context context, ArrayList<MataKuliah> mataKuliahList) {
        this.context = context;
        this.mataKuliahList = mataKuliahList;
    }

    @NonNull
    @Override
    public MataKuliahViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_matakuliah, parent, false);
        return new MataKuliahViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MataKuliahViewHolder holder, int position) {
        final MataKuliah mataKuliah = mataKuliahList.get(position);

        holder.mataKuliahView.setText(mataKuliah.getNamaMataKuliah());
        holder.dosenPengampuView.setText(mataKuliah.getDosenPengampu());

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent detailActivity = new Intent(context, DetailMatkulActivity.class);
                detailActivity.putExtra("ID", mataKuliah.getId());
                context.startActivity(detailActivity);
            }
        });

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent editActivity = new Intent(context, EditMatkulActivity.class);
                editActivity.putExtra("ID", mataKuliah.getId());
                context.startActivity(editActivity);
            }
        });

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseDatabase db = FirebaseDatabase.getInstance("https://ruang-belajar-6570f-default-rtdb.asia-southeast1.firebasedatabase.app/");
                DatabaseReference ref = db.getReference().child("MataKuliah").child(mataKuliah.getId());
                ref.getRef().removeValue();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mataKuliahList.size();
    }

    public class MataKuliahViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout container;
        public TextView mataKuliahView, dosenPengampuView;
        public Button editButton, deleteButton;

        public MataKuliahViewHolder(@NonNull View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.ItemMataKuliahContainer);
            mataKuliahView = itemView.findViewById(R.id.MataKuliahTextView);
            dosenPengampuView = itemView.findViewById(R.id.DosenPengampuTextView);
            editButton = itemView.findViewById(R.id.EditButton);
            deleteButton = itemView.findViewById(R.id.DeleteButton);
        }
    }
}
