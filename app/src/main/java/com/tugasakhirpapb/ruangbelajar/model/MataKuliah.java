package com.tugasakhirpapb.ruangbelajar.model;

public class MataKuliah {
    private String id;
    private String namaMataKuliah;
    private String dosenPengampu;

    public MataKuliah() {

    }

    public MataKuliah(String namaMataKuliah, String dosenPengampu) {
        this.namaMataKuliah = namaMataKuliah;
        this.dosenPengampu = dosenPengampu;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamaMataKuliah() {
        return namaMataKuliah;
    }

    public void setNamaMataKuliah(String namaMataKuliah) {
        this.namaMataKuliah = namaMataKuliah;
    }

    public String getDosenPengampu() {
        return dosenPengampu;
    }

    public void setDosenPengampu(String dosenPengampu) {
        this.dosenPengampu = dosenPengampu;
    }
}
