package com.tugasakhirpapb.ruangbelajar.model;

public class TugasMahasiswa {
    private String id;
    private String tugasID;
    private String userID;
    private String fileurl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTugasID() {
        return tugasID;
    }

    public void setTugasID(String tugasID) {
        this.tugasID = tugasID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getFileurl() {
        return fileurl;
    }

    public void setFileurl(String fileurl) {
        this.fileurl = fileurl;
    }
}
