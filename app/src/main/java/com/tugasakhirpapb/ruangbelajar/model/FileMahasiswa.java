package com.tugasakhirpapb.ruangbelajar.model;

public class FileMahasiswa {
    private String id;
    private String fileName;
    private String fileUrl;
    private String userID;
    private String tugasID;

    public FileMahasiswa() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getTugasID() {
        return tugasID;
    }

    public void setTugasID(String tugasID) {
        this.tugasID = tugasID;
    }
}
