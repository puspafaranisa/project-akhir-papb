package com.tugasakhirpapb.ruangbelajar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.tugasakhirpapb.ruangbelajar.model.FileMahasiswa;
import com.tugasakhirpapb.ruangbelajar.model.Tugas;

public class DetailTugasActivity extends AppCompatActivity {

    public static final int PICKFILE_RESULT_CODE = 1;

    private TextView judulView, deadlineView, descView, tugasStatusView;
    private Button filePickerButton, uploadButton, openButton;
    private ProgressBar progressBar;

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mDBTugasRef;
    private DatabaseReference mDBFileRef;

    private FirebaseStorage mStorage;

    private Uri fileUri;
    private String filePath;

    private Tugas tugas;
    private FileMahasiswa fileMahasiswa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tugas);

        judulView = findViewById(R.id.JudulTugasTextView);
        descView = findViewById(R.id.DescTugasTextView);
        deadlineView = findViewById(R.id.DeadlineTugasTextView);
        filePickerButton = findViewById(R.id.FilePickerButton);
        uploadButton = findViewById(R.id.UploadButton);
        openButton = findViewById(R.id.OpenButton);
        tugasStatusView = findViewById(R.id.TugasStatusTextView);
        progressBar = findViewById(R.id.ProgressBar);

        Intent intent = getIntent();
        String tugasID = intent.getStringExtra("ID");

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        mDatabase = FirebaseDatabase.getInstance("https://ruang-belajar-6570f-default-rtdb.asia-southeast1.firebasedatabase.app/");
        mDBTugasRef = mDatabase.getReference("Tugas").child(tugasID);
        mDBFileRef = mDatabase.getReference("File");

        mStorage = FirebaseStorage.getInstance();

        UpdateUI();
        UpdateFileUI();

        mDBTugasRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                tugas = snapshot.getValue(Tugas.class);
                tugas.setId(snapshot.getKey());
                UpdateUI();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(DetailTugasActivity.this, "Kesalahan Memuat Data", Toast.LENGTH_LONG).show();
            }
        });

        Query fileQuery = mDBFileRef.orderByChild("tugasID").equalTo(tugasID);
        fileQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot data : snapshot.getChildren()) {
                    FileMahasiswa currentFile = data.getValue(FileMahasiswa.class);
                    if (currentFile.getUserID().equals(mUser.getUid())) {
                        fileMahasiswa = currentFile;
                        fileMahasiswa.setId(data.getKey());
                        break;
                    }
                }
                UpdateFileUI();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(DetailTugasActivity.this, "Kesalahan Memuat Data", Toast.LENGTH_LONG).show();
            }
        });

        filePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PilihFile();
            }
        });

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UploadFile();
            }
        });

        openButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fileMahasiswa == null) return;

                mStorage.getReference().child(fileMahasiswa.getFileUrl()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Intent fileIntent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(fileIntent);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Toast.makeText(DetailTugasActivity.this, "Kesalahan Memuat Data", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    private void UpdateUI() {
        if (tugas != null) {
            judulView.setText(tugas.getJudul());
            deadlineView.setText(tugas.getDeadline());
            descView.setText(tugas.getDeskripsi());
        }
    }

    private void UpdateFileUI() {
        if (fileMahasiswa == null) {
            tugasStatusView.setText("Segera Kumpulkan Tugas Anda");
            filePickerButton.setText("Pilih File");
        } else {
            tugasStatusView.setText("Tugas Telah Dikumpulkan");
            filePickerButton.setText("Ganti File");
        }
        uploadButton.setEnabled(false);
        progressBar.setVisibility(View.GONE);
    }

    private void UploadFile() {
        if (filePath == null) {
            Toast.makeText(DetailTugasActivity.this, "Pilih file terlebih dahulu", Toast.LENGTH_SHORT).show();
            return;
        }

        StorageReference storageRef = mStorage.getReference();
        StorageReference fileRef = storageRef.child(tugas.getId() + mAuth.getUid() + fileUri.getLastPathSegment());

        UploadTask uploadTask = fileRef.putFile(fileUri);

        progressBar.setVisibility(View.VISIBLE);
        tugasStatusView.setText("Mengunggah File...");

        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                double progress = (100.0 * snapshot.getBytesTransferred()) / snapshot.getTotalByteCount();
                progressBar.setProgress((int) progress);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(DetailTugasActivity.this, "GAGAL! : " + exception.toString(), Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                FileMahasiswa newFileMahasiswa = new FileMahasiswa();
                newFileMahasiswa.setFileName(taskSnapshot.getMetadata().getName());
                newFileMahasiswa.setFileUrl(taskSnapshot.getMetadata().getPath());
                newFileMahasiswa.setTugasID(tugas.getId());
                newFileMahasiswa.setUserID(mUser.getUid());
                if (fileMahasiswa == null) {
                    mDBFileRef.push().setValue(newFileMahasiswa);
                } else {
                    mDBFileRef.child(fileMahasiswa.getId()).setValue(newFileMahasiswa);
                }
                Toast.makeText(DetailTugasActivity.this, "FILE BERHASIL DI UPLOAD", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void PilihFile() {
        String[] mimetypes = {
                "application/*",
                "image/*",
                "text/*",
        };

        Intent chooseFileActivity = new Intent(Intent.ACTION_GET_CONTENT);
        chooseFileActivity.setType("*/*");
        chooseFileActivity.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        chooseFileActivity = Intent.createChooser(chooseFileActivity, "Choose a file");
        startActivityForResult(chooseFileActivity, PICKFILE_RESULT_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICKFILE_RESULT_CODE:
                if (resultCode == -1) {
                    fileUri = data.getData();
                    filePath = fileUri.getPath();
                    filePickerButton.setText("(Klik Untuk Ganti File) \n" + filePath);
                    uploadButton.setEnabled(true);
                }
                break;
        }
    }
}