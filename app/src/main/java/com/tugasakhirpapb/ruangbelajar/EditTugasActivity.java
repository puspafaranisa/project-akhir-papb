package com.tugasakhirpapb.ruangbelajar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tugasakhirpapb.ruangbelajar.model.Tugas;

public class EditTugasActivity extends AppCompatActivity {

    EditText judulInput, deadlineInput, deskripsiInput;
    Button editButton;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mDBTugasRef;

    private Tugas tugas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_tugas);

        judulInput = findViewById(R.id.JudulInput);
        deadlineInput = findViewById(R.id.DeadlineInput);
        deskripsiInput = findViewById(R.id.DeskripsiInput);
        editButton = findViewById(R.id.EditButton);editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateKelas();
            }
        });

        Intent intent = getIntent();
        String tugasID = intent.getStringExtra("ID");

        //INSTANSIASI FIREBASE DATABASE
        mDatabase = FirebaseDatabase.getInstance("https://ruang-belajar-6570f-default-rtdb.asia-southeast1.firebasedatabase.app/");

        //REFERENSI DATABASE KE TABEL MATAKULIAH
        mDBTugasRef = mDatabase.getReference("Tugas").child(tugasID);

        mDBTugasRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                tugas = snapshot.getValue(Tugas.class);
                tugas.setId(snapshot.getKey());
                UpdateUI();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(EditTugasActivity.this, "Kesalahan Memuat Data", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void UpdateUI() {
        judulInput.setText(tugas.getJudul());
        deadlineInput.setText(tugas.getDeadline());
        deskripsiInput.setText(tugas.getDeskripsi());
    }

    private void UpdateKelas() {
        tugas.setJudul(judulInput.getText().toString());
        tugas.setDeadline(deadlineInput.getText().toString());
        tugas.setDeskripsi(deskripsiInput.getText().toString());

        mDBTugasRef.setValue(tugas);
        Toast.makeText(EditTugasActivity.this, "Data Berhasil Di Ubah", Toast.LENGTH_LONG).show();
        finish();
    }
}