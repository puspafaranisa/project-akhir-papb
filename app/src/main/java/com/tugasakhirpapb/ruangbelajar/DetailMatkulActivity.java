package com.tugasakhirpapb.ruangbelajar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tugasakhirpapb.ruangbelajar.R;
import com.tugasakhirpapb.ruangbelajar.controller.TugasAdapter;
import com.tugasakhirpapb.ruangbelajar.model.MataKuliah;
import com.tugasakhirpapb.ruangbelajar.model.Tugas;

import java.util.ArrayList;

public class DetailMatkulActivity extends AppCompatActivity {

    private TextView namaMataKuliahTextView, dosenPengampuTextView;
    private RecyclerView recyclerViewTugas;
    private TugasAdapter tugasAdapter;
    private Button tambahTugasButton;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mDBMatkulRef, mDBTugasRef;

    private MataKuliah mataKuliah;

    private ArrayList<Tugas> tugasList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_matkul);

        namaMataKuliahTextView = findViewById(R.id.MataKuliahTextView);
        dosenPengampuTextView = findViewById(R.id.DosenPengampuTextView);
        recyclerViewTugas = findViewById(R.id.RecycleViewTugas);
        tambahTugasButton = findViewById(R.id.TambahButton);

        tugasAdapter = new TugasAdapter(DetailMatkulActivity.this, tugasList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DetailMatkulActivity.this);

        recyclerViewTugas.setLayoutManager(layoutManager);
        recyclerViewTugas.setAdapter(tugasAdapter);

        Intent intent = getIntent();
        String matkulID = intent.getStringExtra("ID");

        //INSTANSIASI FIREBASE DATABASE
        mDatabase = FirebaseDatabase.getInstance("https://ruang-belajar-6570f-default-rtdb.asia-southeast1.firebasedatabase.app/");

        //REFERENSI DATABASE KE TABEL MATAKULIAH
        mDBMatkulRef = mDatabase.getReference("MataKuliah").child(matkulID);
        mDBTugasRef = mDatabase.getReference("Tugas");

        UpdateUI();

        tambahTugasButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent tambahActivity = new Intent(DetailMatkulActivity.this, TambahTugasActivity.class);
                tambahActivity.putExtra("ID", mataKuliah.getId());
                startActivity(tambahActivity);
            }
        });

        mDBMatkulRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                mataKuliah = snapshot.getValue(MataKuliah.class);
                mataKuliah.setId(snapshot.getKey());
                UpdateUI();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(DetailMatkulActivity.this, "Kesalahan Memuat Data", Toast.LENGTH_LONG).show();
            }
        });


    }

    private void UpdateUI() {
        if (mataKuliah != null) {
            namaMataKuliahTextView.setText(mataKuliah.getNamaMataKuliah());
            dosenPengampuTextView.setText(mataKuliah.getDosenPengampu());

            Query tugasQuery = mDBTugasRef.orderByChild("idMatkul").equalTo(mataKuliah.getId());

            tugasQuery.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    tugasList.clear();
                    for (DataSnapshot data : snapshot.getChildren()) {
                        Tugas tugas = data.getValue(Tugas.class);
                        tugas.setId(data.getKey());
                        tugasList.add(tugas);
                    }
                    tugasAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(DetailMatkulActivity.this, "Kesalahan Memuat Data", Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}