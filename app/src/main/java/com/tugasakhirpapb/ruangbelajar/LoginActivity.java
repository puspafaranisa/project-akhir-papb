package com.tugasakhirpapb.ruangbelajar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    private EditText emailInput, passwordInput;
    private Button loginButton;
    private TextView registerTextButton;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //MENCARI VIEW
        emailInput = findViewById(R.id.EmailTextInput);
        passwordInput = findViewById(R.id.PasswordITextnput);
        loginButton = findViewById(R.id.LoginButton);
        registerTextButton = findViewById(R.id.RegisterTextButton);

        //INSTANSIASI FIREBASE AUTH
        mAuth = FirebaseAuth.getInstance();

        //KLIK TOMBOL LOGIN
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestLogin();
            }
        });

        //KLIK TOMBOL REGISTER SEKARANG
        registerTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GoToRegisterActivity();
            }
        });
    }

    private void RequestLogin() {
        //AMBIL DATA DARI TEXT INPUT
        String email = emailInput.getText().toString();
        String password = passwordInput.getText().toString();

        //CEK DATANYA APAKAH ADA ISI
        if (email == null) {
            Toast.makeText(LoginActivity.this, "Email Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (password == null) {
            Toast.makeText(LoginActivity.this, "Password Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else {
            //REGISTER AKUN BARU
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                //SAAT REGISTER BERHASIL
                                Toast.makeText(LoginActivity.this, "Login Berhasil", Toast.LENGTH_SHORT).show();

                                Intent mainActivity = new Intent(LoginActivity.this, MainActivity.class);
                                mainActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                startActivity(mainActivity);
                                LoginActivity.this.finish();
                            } else {
                                Toast.makeText(LoginActivity.this, "Kesalahan Saat Login : " + task.getException(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
    }

    private void GoToRegisterActivity() {
        //GANTI ACTIVITY KE REGISTER
        Intent registerActivity = new Intent(LoginActivity.this, RegisterActivity.class);
        registerActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        startActivity(registerActivity);
        this.finish();
    }
}