package com.tugasakhirpapb.ruangbelajar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tugasakhirpapb.ruangbelajar.model.Tugas;

public class TambahTugasActivity extends AppCompatActivity {

    EditText judulInput, deadlineInput, deskripsiInput;
    Button tambahButton;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mDBTugasRef;

    private String matkulID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_tugas);

        judulInput = findViewById(R.id.JudulInput);
        deadlineInput = findViewById(R.id.DeadlineInput);
        deskripsiInput = findViewById(R.id.DeskripsiInput);
        tambahButton = findViewById(R.id.TambahButton);

        //INSTANSIASI FIREBASE DATABASE
        mDatabase = FirebaseDatabase.getInstance("https://ruang-belajar-6570f-default-rtdb.asia-southeast1.firebasedatabase.app/");

        //REFERENSI DATABASE KE TABEL TUGAS
        mDBTugasRef = mDatabase.getReference("Tugas");

        Intent intent = getIntent();
        matkulID = intent.getStringExtra("ID");

        tambahButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TambahTugas();
            }
        });
    }

    private void TambahTugas() {
        String judul = judulInput.getText().toString();
        String deadline = deadlineInput.getText().toString();
        String desc = deskripsiInput.getText().toString();

        if (judul == null) {
            Toast.makeText(TambahTugasActivity.this, "Judul Tugas Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (deadline == null) {
            Toast.makeText(TambahTugasActivity.this, "Deadline  Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (desc == null) {
            Toast.makeText(TambahTugasActivity.this, "Deskripsi Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else {
            Tugas newTugas = new Tugas();
            newTugas.setJudul(judul);
            newTugas.setDeadline(deadline);
            newTugas.setDeskripsi(desc);
            newTugas.setIdMatkul(matkulID);
            mDBTugasRef.push().setValue(newTugas);
            Toast.makeText(TambahTugasActivity.this, "Kelas Berhasil Ditambahkan", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}